package org.ihomeland.vpn.widget.tab;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.ihomeland.vpn.R;


/**
 * The Button in the TabHost, which displays Tab title
 */

public class TabButton extends RelativeLayout {
    private ImageView image;
    private TextView tab_button;

    private boolean isChecked = false;

    public TabButton(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        initLayout(context, attrs);
    }

    public TabButton(Context context)
    {
        super(context);
    }

    public TabButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initLayout(context, attrs);
    }

    private void initLayout(Context context, AttributeSet attrs)
    {
        View contentView = LayoutInflater.from(context).inflate(R.layout.tab_btn_bottom, this);
        image = (ImageView) contentView.findViewById(R.id.iv_tab_item_icon);
        tab_button = (TextView) contentView.findViewById(R.id.tv_tab_item_icon);
        TypedArray a = getResources().obtainAttributes(attrs, R.styleable.tab_button);
        Drawable d = a.getDrawable(R.styleable.tab_button_drawableTop);
        String text = a.getString(R.styleable.tab_button_tabtext);
        String attrStr = a.getString(R.styleable.tab_button_drawableTopAttr);
        a.recycle();
        tab_button.setText(text);
        if (d != null)
        {
            image.setImageDrawable(d);
        } else
        {
            TypedValue typedValue = new TypedValue();
            getContext().getTheme().resolveAttribute(getResources().getIdentifier(attrStr, "attr", context.getPackageName()),
                    typedValue, true);
            image.setImageResource(typedValue.resourceId);
        }

    }

}
