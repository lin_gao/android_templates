package org.ihomeland.vpn.widget.tab;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

/**
 * TabHost represents a TabLayout, Children components can be TabButton or TabTextView
 */

public class TabHost extends LinearLayout implements OnClickListener {

    public TabHost(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View view = getChildAt(i);
            if (view instanceof TabButton || view instanceof TabTextView) {
                view.setOnClickListener(this);
            }
        }
    }

    public void setChecked(int position) {
        setChecked(position, false);
    }

    private void setChecked(int position, boolean byUser) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View view = getChildAt(i);
            if (view instanceof TabButton || view instanceof TabTextView) {
                view.setSelected(position == i);
                if (position == i && mListener != null) {
                    mListener.onCheckedChange(position, byUser);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View view = getChildAt(i);
            if (v == view) {
                setChecked(i, true);
                break;
            }
        }
    }

    private OnCheckedChangeListener mListener;

    public void setOnCheckedChangeListener(OnCheckedChangeListener mListener) {
        this.mListener = mListener;
    }

    public interface OnCheckedChangeListener {
        void onCheckedChange(int checkedPosition, boolean byUser);
    }
}
