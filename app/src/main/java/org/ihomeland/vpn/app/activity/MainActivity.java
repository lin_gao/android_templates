package org.ihomeland.vpn.app.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.ihomeland.vpn.R;
import org.ihomeland.vpn.widget.tab.FragmentBuy;
import org.ihomeland.vpn.widget.tab.FragmentConn;
import org.ihomeland.vpn.widget.tab.FragmentHelp;
import org.ihomeland.vpn.widget.tab.FragmentProfile;
import org.ihomeland.vpn.widget.tab.TabAdapter;
import org.ihomeland.vpn.widget.tab.TabHost;
import org.ihomeland.vpn.widget.tab.TabViewPager;

public class MainActivity extends FragmentActivity implements ViewPager.OnPageChangeListener, TabHost.OnCheckedChangeListener {

    TextView txt_title;
    ImageView img_right;
    TabHost tabHost;
    TabViewPager viewpager;

    private TabAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindComponents();
        initMainTab();
    }

    private void bindComponents() {
        txt_title = (TextView)findViewById(R.id.txt_title);
        img_right = (ImageView)findViewById(R.id.img_right);
        tabHost = (TabHost)findViewById(R.id.tab_host);
        viewpager = (TabViewPager)findViewById(R.id.viewpager);
    }

    private void initMainTab() {
        tabHost.setOnCheckedChangeListener(this);
        tabHost.setChecked(0);
        adapter = new TabAdapter(getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        adapter.clear();
        viewpager.setScrollable(false);
        viewpager.setOffscreenPageLimit(4);
        adapter.addFragment(new FragmentConn(), getString(R.string.tabConn));
        adapter.addFragment(new FragmentBuy(), getString(R.string.tabBuy));
        adapter.addFragment(new FragmentHelp(), getString(R.string.tabHelp));
        adapter.addFragment(new FragmentProfile(), getString(R.string.tabProfile));
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onCheckedChange(int checkedPosition, boolean byUser) {
        setTabMsg(checkedPosition);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        tabHost.setChecked(position);
        setTabMsg(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void setTabMsg(int checkedPosition) {
        viewpager.setCurrentItem(checkedPosition);
        switch (checkedPosition) {
            case 0:
                img_right.setVisibility(View.VISIBLE);
                img_right.setImageResource(R.drawable.icon_add);
//                txt_title.setText(getString(R.string.tabConn));
                break;
            case 1:
                img_right.setVisibility(View.VISIBLE);
                img_right.setImageResource(R.drawable.icon_titleaddfriend);
//                txt_title.setText(getString(R.string.tabBuy));
                break;
            case 2:
                img_right.setVisibility(View.GONE);
//                txt_title.setText(getString(R.string.tabHelp));
                break;
            case 3:
                img_right.setVisibility(View.GONE);
//                txt_title.setText(getString(R.string.tabProfile));
                break;
        }
    }
}
